<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180905040519 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("INSERT INTO tariff (`id`, `name`, `price`, `day_of_week`) 
            VALUES 
                (1, 'Тариф 1', '234','a:2:{i:0;s:6:\"Friday\";i:1;s:8:\"Thursday\";}'), 
                (2, 'Тариф 2', '123','a:1:{i:0;s:6:\"Monday\";}'),
                (3, 'Тариф 3', '456','a:2:{i:0;s:7:\"Tuesday\";i:1;s:8:\"Saturday\";}')
            ");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("DELETE FROM tariff WHERE `id` BETWEEN 1 and 3");

    }
}
