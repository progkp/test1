Platform:
 PHP 7.1
 Symfony 3.4
 MySQL 5.6
 
Instruction:
1) clone the project into desired directory
2) run
```
composer install
```
3) Create database
```
bin/console doctrine:database:create
``` 
4) Run migrations
```
bin/console doctrine:migrations:migrate
```
5) Clear cache
```
bin/console cache:clear --env=prod --no-debug --no-warmup
```
