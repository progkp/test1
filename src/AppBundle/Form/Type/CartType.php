<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Cart;
use AppBundle\Entity\Tariff;
use AppBundle\Validator\Constraints\DayStart;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Valid;

/**
 * Class CartType
 * @package AppBundle\Form\Type
 */
class CartType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user', UserType::class, [
                'constraints' => [
                    new Valid(),
                ],
            ])
            ->add('start_day', DateType::class, [
                'widget'      => 'single_text',
                'label'       => 'form.cart.day_start',
                'format'      => 'yyyy-MM-dd',
                'html5'       => false,
                'attr'        => [
                    'class' => 'js-datepicker',
                ],
                'constraints' => [
                    new DayStart(),
                ],
            ])
            ->add('address', TextType::class, [
                'label' => 'form.cart.address',
            ])
            ->add('tariff', EntityType::class, [
                'class'        => Tariff::class,
                'label'        => 'form.cart.tariff',
                'choice_label' => function (Tariff $tariff) {
                    return $tariff->getChoiceLabel();
                },
            ])
            ->add('save', SubmitType::class, [
                'label' => 'form.cart.button',
            ]);

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Cart::class,
        ]);

    }

}