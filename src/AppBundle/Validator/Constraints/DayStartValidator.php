<?php

namespace AppBundle\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class DayStartValidator
 * @package AppBundle\Validator\Constraints
 */
class DayStartValidator extends ConstraintValidator
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * DayStartValidator constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param mixed $object
     * @param Constraint $constraint
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function validate($object, Constraint $constraint)
    {
        $tariff = $this->context->getRoot()->getConfig()->getData()->getTariff()->getId();

        $dayStart = $this->em->getRepository('AppBundle:Tariff')->checkDayStart($object, $tariff);

        if (!$dayStart) {
            $this->context->addViolation('Not delivered on this day of the week.');
        }

    }

}