<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\TariffRepository")
 * @ORM\Table(name="tariff")
 */
class Tariff
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var float
     * @ORM\Column(name="price", type="float")
     */
    private $price;

    /**
     * @var array
     * @ORM\Column(name="day_of_week", type="array")
     */
    private $dayOfWeek;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\Cart",
     *     mappedBy="tariff"
     * )
     */
    private $cart;

    /**
     * Tariff constructor.
     */
    public function __construct()
    {
        $this->cart = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Tariff
     */
    public function setName(string $name): Tariff
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param int $price
     * @return Tariff
     */
    public function setPrice(int $price): Tariff
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return array
     */
    public function getDayOfWeek(): array
    {
        return $this->dayOfWeek;
    }

    /**
     * @param array $dayOfWeek
     * @return Tariff
     */
    public function setDayOfWeek(array $dayOfWeek): Tariff
    {
        $this->dayOfWeek = $dayOfWeek;

        return $this;
    }

    /**
     * @return string
     */
    public function getChoiceLabel()
    {
        return $this->getName().' (День доставки: '.implode(", ", $this->getDayOfWeek()).')';
    }

}