<?php

namespace AppBundle\Entity\Repository;

use AppBundle\Entity\Tariff;
use Doctrine\ORM\EntityRepository;

/**
 * Class TariffRepository
 * @package AppBundle\Entity\Repository
 */
class TariffRepository extends EntityRepository
{
    /**
     * @param \DateTime $date
     * @param $tariff
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function checkDayStart(\DateTime $date, $tariff)
    {

        $dayOfWeek = date("l", $date->getTimestamp());

        $qb = $this->createQueryBuilder('t');
        $exp = $qb->expr();

        $qb
            ->where($exp->eq('t.id', ':tariff'))
            ->setParameters(['tariff' => $tariff]);

        /** @var Tariff $result */
        $result = $qb->getQuery()->getOneOrNullResult();

        if (in_array($dayOfWeek, $result->getDayOfWeek())) {
            return true;
        }

        return false;
    }
}