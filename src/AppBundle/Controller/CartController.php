<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Cart;
use AppBundle\Form\Type\CartType;
use AppBundle\Helper\ControllerHelperTrait;
use AppBundle\Manager\CartManager;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CartController
 * @package AppBundle\Controller
 */
class CartController extends FOSRestController
{

    Use ControllerHelperTrait;

    /**
     * @var CartManager
     */
    protected $cartManager;

    /**
     * CartController constructor.
     * @param CartManager $cartManager
     */
    public function __construct(CartManager $cartManager)
    {
        $this->cartManager = $cartManager;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route(
     *     "/",
     *     name="api_cart_index",
     *     methods={"GET"}
     * )
     */
    public function indexCartAction()
    {

        $form = $this->createForm(CartType::class, null, [
            'action' => $this->generateUrl('api_cart_post'),
            'method' => 'POST',
        ]);

        return $this->render('@App/index.html.twig', [
            'form' => $form->createView(),
        ]);

    }

    /**
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     *
     * @Route(
     *     "api/cart",
     *     name="api_cart_post",
     *     methods={"POST"}
     * )
     */
    public function postCartAction(Request $request)
    {
        try {

            $cart = new Cart();

            $form = $this->createForm(CartType::class, $cart);
            $form->handleRequest($request);

            if ($form->isSubmitted()) {

                if ($form->isValid()) {

                    $this->cartManager->createCart($cart);

                    return $this->created();

                } else {
                    return $this->badParam($form);
                }
            }

            return $this->ok();

        } catch (\Exception $e) {
            return $this->badRequest($e->getMessage());
        }

    }

}