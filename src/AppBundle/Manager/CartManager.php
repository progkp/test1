<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Cart;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class CartManager
 * @package AppBundle\Manager
 */
class CartManager extends BaseManager
{

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * CartManager constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
    }

    /**
     * @param Cart $cart
     */
    public function createCart(Cart $cart)
    {

        if (!$user = $this->em->getRepository(User::class)->findOneBy(['phone' => $cart->getUser()->getPhone()])) {
            $user = new User();
            $user->setName($cart->getUser()->getName());
            $user->setPhone($cart->getUser()->getPhone());
        }

        $cart->setUser($user);
        $this->save($cart);
    }

}