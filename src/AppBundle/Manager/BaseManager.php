<?php

namespace AppBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Class BaseManager
 * @package AppBundle\Manager
 */
class BaseManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * BaseManager constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param $entity
     * @return mixed
     */
    public function save($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();

        return $entity;
    }
}