<?php

namespace AppBundle\Helper;

use FOS\RestBundle\View\View;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Trait ControllerHelperTrait
 * @package AppBundle\Helper
 */
trait ControllerHelperTrait
{

    /**
     * @param $data
     * @return View
     */
    public function ok($data = 'Ok'): View
    {

        return View::create([
            'data' => $data,
        ], Response::HTTP_OK);
    }

    /**
     * @return View
     */
    public function notFound(): View
    {
        return View::create([
            'error' => [
                'type'    => 'not_found',
                'message' => 'Not found',
            ],
        ], Response::HTTP_NOT_FOUND);
    }

    /**
     * @param FormInterface $form
     * @return View
     */
    public function badParam(FormInterface $form): View
    {

        $errors = [];
        foreach ($form->getErrors(true, true) as $index => $formError) {
            $errors[$index]['message'] = $formError->getMessage();
            $errors[$index]['name'] = $formError->getOrigin()->getConfig()->getName();
        }

        return View::create([
            'error' => [
                'type'    => 'params',
                'message' => 'Bad params',
                'params'  => $errors,
            ],
        ], Response::HTTP_OK);
    }

    /**
     * @param string $error
     * @return View
     */
    public function badRequest($error): View
    {
        return View::create([
            'error' => [
                'type'    => 'system',
                'message' => $error,
            ],
        ], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @param string $data
     * @return View
     */
    public function created($data = 'Ok'): View
    {
        return View::create([
            'data' => [
                'status'  => true,
                'message' => $data,
            ],
        ], Response::HTTP_CREATED);
    }

}